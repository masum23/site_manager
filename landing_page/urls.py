from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^(?:(?P<slug>(privacy|terms|thanks|index-amp|privacy-amp|terms-amp))/)?$', views.index, name='index'),
]
